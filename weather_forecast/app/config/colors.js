export default {
	primary: "#f58433", // Orange.
	secondary: "#282c34", // Black.
	black: "#000",
	white: "#fff",
	medium: "#999",
	light: "#f8f4f4",
	dark: "#0c0c0c",
	forest_sunny:"#47AB2F",
	forest_cloudy:"#54717A",
	forest_rainy:"#57575D"
};
