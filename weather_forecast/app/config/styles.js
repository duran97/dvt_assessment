import { Platform } from "react-native";
//import { GetPlatformElevation } from "../utils";

import colors from "./colors";

export default {
	colors,
	text: {
		color: colors.dark,
		fontSize: 18,
		fontFamily: Platform.OS === "android" ? "Roboto" : "Avenir",
	},
	elevatedListItem: {
		marginTop: 5,
		marginLeft: 10,
		marginRight: 10,
		borderRadius: 5,
		overflow: "hidden",
		marginBottom: 5,
		//...GetPlatformElevation(4),
	},
};
