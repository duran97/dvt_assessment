import React , {useEffect,useState} from "react";
import { RefreshControl, StyleSheet,TouchableOpacity,View,Image ,FlatList,Text,TouchableHighlight} from "react-native";
import {GetAllUserFavourites} from '../../utils/databaseOperation';
import { useNavigation } from '@react-navigation/native';
import Spinner from 'react-native-loading-spinner-overlay';
import AppScreen from '../../components/AppScreen';
import AppText from '../../components/AppText';

const Feature_2 = () => {
	const navigation = useNavigation();
	const [state, setState] = useState({
		data: [],
		isLoading:false,
		refreshing:false
	  })

	  useEffect(() => {
   
		SetNavigationOptions()
		

		return () => {
		};
	  }, []);

	  ListEmpty = () => {
		return (
		  //View to show when list is empty
		  <View style={[{marginTop:5}]}>
			<Text style={{ textAlign: 'center' }}>{ 'No Favourites added'}</Text>
			<Text style={{ textAlign: 'center' }}>{ 'Please pull to refresh'}</Text>
		  </View>
		);
	  };

	  function RetrieveFavourites (){
		// Retrieve a list of all favourites stored locally.
		GetAllUserFavourites().then(async function (allFavourites) {
			setState({...state,data:allFavourites,isLoading:false})
		}).catch(function (error) {
			alert(error)
		})
	  }

	  function SetNavigationOptions(){
		navigation.setOptions({
		    headerLeft: () => (
			<TouchableHighlight
				underlayColor="white"
				onPress={() => navigation.openDrawer()}	
				style={{width:80,height:40,marginTop:15,alignItems:'flex-start'}}					  
			>
				<View>
					<Image style={{width:80,height:30}} source={require('../../../assets/hamburger.png')}></Image>
				</View>
			</TouchableHighlight>	
			),
		});
	  }

	  const pullToRrefresh = async () => {
		setState({...state,refreshing:true});
		RetrieveFavourites();
	  }
	
  return (
    <AppScreen style={styles.container}>
	  <Spinner
		visible={state.isLoading}
		textContent={'Loading...'}
	 />   
	 <FlatList
		data={state.data}
		ListEmptyComponent={ListEmpty}
		refreshControl={
            <RefreshControl
              refreshing={state.refreshing}
              onRefresh={pullToRrefresh.bind(this)}
            />}
		renderItem={({ item, index }) => (
			<TouchableOpacity>
				<View style={styles.listItem}>
				<Image
					style={styles.image}
					source={require('../../../assets/location.png')}>
                </Image>
				<View style={{ alignItems: "center", flex: 1 }}>
					<AppText style={styles.title}>{item.City}</AppText>
					<AppText style={styles.title}>{item.Country}</AppText>
				</View>							
				</View>
			</TouchableOpacity>		
				)}
				keyExtractor={(item) => item.Id.toString()}
			/>	
    </AppScreen>
  );
};

const styles = StyleSheet.create({
  container: {},
  listItem: {
		width: "95%",
		padding: 15,
		backgroundColor: "#FFF",
		alignSelf: "center",
		flexDirection: "row",
		borderRadius: 10,
		elevation: 1,
		marginVertical: 8,
		marginHorizontal: 16,
	},
	title: {
		fontSize: 20,
		padding: 5,
		alignSelf:'flex-start'
	},
	image: {
		width: 60,
		height: 60,
		borderRadius: 30,
		alignSelf: "center",
	},
	TouchableOpacity: {
		justifyContent: "center",
		alignItems: "center",
		marginVertical: 10,
	},
	floatingContainer: {
		position: "absolute",
		top: 0,
		left: 0,
		right: 10,
		bottom: 10,
		justifyContent: "flex-end",
		alignItems: "flex-end",
	},
});

export default Feature_2;
