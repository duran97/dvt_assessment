import React,{useState,useEffect} from "react";
import Spinner from 'react-native-loading-spinner-overlay';
import {  StyleSheet,ImageBackground,Dimensions,View ,FlatList,Image,TouchableHighlight,PermissionsAndroid} from "react-native";
import { useNavigation } from '@react-navigation/native';
import moment from 'moment';
import NetInfo from "@react-native-community/netinfo";
import DialogInput from 'react-native-dialog-input';

//Components
import {InsertFavourite,InsertCurrentForecast,GetCurrentForecast} from '../../utils/databaseOperation';
import {GetUserCurrentLocation} from '../../utils';
import AppScreen from '../../components/AppScreen';
import AppText from '../../components/AppText';
import colors from "../../config/colors";
import utils from '../../utils';

const Home = () => {
  const nav = useNavigation();
  const [state, setState] = useState({
    data: [],
    currentDayWeatherForecast:{},
    weatherImageBackground:'',
    screenBackgroundColor:'',
    isLoading:false,
    location:null,
    lastTimeUpdated : '',
    permissionGranted:false,
    isDialogVisible:false

  })

  useEffect(() => {
  
    SetNavigationOptions();

     // Check if network is connected on the app and location permissions.
     GetLocationPermission()
     
     NetInfo.fetch().then(state => {
          if(state.isConnected === true){
            
            SetScreenData()
          }
          else{
    
            // Retrieve local storage forecast saved, when user is offline.
            GetCurrentForecast().then(function (currentForecast){
              
                let screenResources = SetScreenResources(currentForecast);
        
                let weatherForecast5Days = currentForecast.WeekForecast;
      
                SetDataValues(currentForecast,screenResources,weatherForecast5Days)
      
            }).catch(function (error) { 
              alert(error)
            })
          }
      });
     
    
    return () => {
    };
  }, []);

  const GetLocationPermission = async () => {
    // Request permissions
    const hasLocationPermission = await RequestLocationPermission();
  
    if (!hasLocationPermission) return;
  }

  async function RequestLocationPermission  () {
    const chckLocationPermission = PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION);
        if (chckLocationPermission === PermissionsAndroid.RESULTS.GRANTED) {
            // Permission granted resume app operation
            return;
        } else {
            try {
                const granted = await PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
                    {
                        'title': 'This App required Location permission',
                        'message': 'We required Location permission in order to get device location ' +
                            'Please grant us.'
                    }
                )
                if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                   return;
                } else {
                    alert("You don't have access for the location");
                    setState({...state,isLoading:false})
                }
            } catch (err) {
                alert(err)
                setState({...state,isLoading:false})
            }
        }
  }

  function InsertFavouritesToLocalStorage (){

      // Retrieve existing forecast from sqlite table and insert into favourites table.
      GetCurrentForecast().then(function (currentForecast){
                  
        // Insert favourites into local storage. 
        let favourteObj = {
          Id:new Date().getTime() + (Math.floor(Math.random() * Math.floor(new Date().getTime()))),
          Country:currentForecast.Country,
          City:currentForecast.City,
          latitude:currentForecast.Latitude,
          longitude:currentForecast.Longitude
        }

        InsertFavourite(favourteObj).then(async function () {

          alert('Favourite added. See your favourites.')
          
        }).catch(function (error) {
          console.log('InsertFavourite', error)
        })

      }).catch(function (error) { 
      console.log('GetCurrentForecast', error);
      })

     
    }
    
  const InsertLocalStorageCurrentForecast = async (currentForecast,weather5DayForecast) => {
    InsertCurrentForecast(currentForecast,weather5DayForecast).then().catch(function (error) {
      console.log('InsertCurrentForecast', error);
    })
  }

  function Refresh(){
     // Check if network is connected on the app.
     NetInfo.fetch().then(state => {
     if(state.isConnected){
      SetScreenData();
     }
     else{

      // Retrieve local storage forecast saved, when user is offline.
      GetCurrentForecast().then(function (currentForecast){
        
         let screenResources = SetScreenResources(currentForecast);
 
         let weatherForecast5Days = currentForecast.WeekForecast;

         SetDataValues(currentForecast,screenResources,weatherForecast5Days)

      }).catch(function (error) { 
        console.log('GetCurrentForecast', error);
      })
     }
    })
  }

  function SetNavigationOptions(){
    nav.setOptions({
      headerRight: () => (
          <View style={{flexDirection:'row'}}>
              <TouchableHighlight
               underlayColor="#DDDDDD"
               onPress={() => OpenSearchDialog()}						  
             >
             <View>
               <Image style={{width:20,height:20,marginRight:10,marginTop:5}} source={require('../../../assets/search.png')}></Image>
             </View>
             </TouchableHighlight>
             <TouchableHighlight
               underlayColor="#DDDDDD"
               onPress={() => Refresh()}						  
             >
             <View>
               <Image style={{width:30,height:30,marginRight:10}} source={require('../../../assets/refresh.png')}></Image>
             </View>
             </TouchableHighlight>
             <TouchableHighlight
               underlayColor="#DDDDDD"
               onPress={() => InsertFavouritesToLocalStorage()}						  
             > 
             <View>
               <Image style={{width:20,height:20,marginRight:20,marginTop:5}} source={require('../../../assets/favourite.png')}></Image>
             </View>
             </TouchableHighlight>
            </View>
             ),
     });
  }

  function OpenSearchDialog(){ 
    setState({...state,isDialogVisible:true}) 
  }

  async function SetScreenData (citySearchResult = ''){
    
        setState({...state,isLoading:true})

        GetUserCurrentLocation().then(async function (location) {

        //Retrieve data from api call of the weather for the current day.
        let currentForecast = '';

        if(citySearchResult === ''){
          currentForecast = await utils.GetCurrentWeatherForecast(location.longitude,location.latitude);
        }
        else{
          currentForecast = await utils.GetCitySearchResult(citySearchResult);
        }
       
        let screenResources = SetScreenResources(currentForecast);

        let weatherForecast5Days = await utils.Get5DayForecast(location.longitude,location.latitude,screenResources.forecastIcon);

        // Save values to local storage, with time stamp.
        InsertLocalStorageCurrentForecast(currentForecast,weatherForecast5Days);

        // Set data values from api call.
        SetDataValues(currentForecast,screenResources,weatherForecast5Days);
      
      }).catch(function (error) {
        alert(error)
      }) 
  }

  const SetDataValues = async (currentForecast,screenResources,weatherForecast5Days) => {

    let currentTemperature ='';
    let description = '';
    let minTemperature = '';
    let maxTemperature ='';
    let cityName = '';
    let longitude = '';
    let latitude = '';
    let country = '';
    let lastUpdated = moment().format('MMMM Do YYYY, h:mm:ss a');

    // Assign values based on api call or sqlite. Api call has different structure.
    if(currentForecast.main === undefined){
      currentTemperature = currentForecast.CurrentTemperature;
      description = currentForecast.Description;
      minTemperature = currentForecast.MinTemperature;
      maxTemperature = currentForecast.MaxTemperature;
      cityName = currentForecast.City;
      longitude = currentForecast.longitude;
      latitude = currentForecast.latitude;
      country = currentForecast.Country;
      lastUpdated = currentForecast.LastTimeUpdated;
    }
    else{
      // Values retrieved from api call.
      currentTemperature = currentForecast.main.temp;
      description = currentForecast.weather[0].main;
      minTemperature = currentForecast.main.temp_min;
      maxTemperature = currentForecast.main.temp_max;
      cityName = currentForecast.name;
      longitude = currentForecast.coord.lon;
      latitude = currentForecast.coord.lat;
      country = currentForecast.sys.country;
      lastUpdated = lastUpdated;
    }

    setState({...state,
      currentDayWeatherForecast:{
        CurrentTemperature:Math.round(currentTemperature),
        Description : description,
        MinTemperature :Math.round(minTemperature),
        MaxTemperature : Math.round(maxTemperature),
        City : cityName,
        Country:country,
        Longitude : longitude,
        Latitude :  latitude},
      weatherImageBackground:screenResources.imageBackground,
      screenBackgroundColor:screenResources.backgroundColor,
      data: weatherForecast5Days,
      lastTimeUpdated:lastUpdated,
      isLoading:false,
      isDialogVisible:false
    })
  }
  
  function SetScreenResources(currentForecast){

     let weatherDescription = '';

     // api call and sqlite have different field structure.
     // extract the correct fields and save to common variable.
     if(currentForecast.weather === undefined){
        weatherDescription = currentForecast.Description;
     }
     else{
       weatherDescription = currentForecast.weather[0].main;
     }
     // Set resources depending on api data returned.
     let resourcesObj = {
      imageBackground : '',
      backgroundColor : '',
      forecastIcon : ''
     }
     
     if(weatherDescription === 'Clouds' || weatherDescription === 'Thunderstorm'){
       resourcesObj.imageBackground = require('../../../assets/forest_cloudy.png');
       resourcesObj.backgroundColor = colors.forest_cloudy;
       resourcesObj.forecastIcon = 'partlySunny';
     }
     else if(weatherDescription === 'Rain'){
      resourcesObj.imageBackground = require('../../../assets/forest_rainy.png');
      resourcesObj.backgroundColor = colors.forest_rainy;
      resourcesObj.forecastIcon = 'rain';
     }
     else if(weatherDescription === 'Clear'){
      resourcesObj.imageBackground = require('../../../assets/forest_sunny.png');
      resourcesObj.backgroundColor = colors.forest_sunny;
      resourcesObj.forecastIcon = 'clear';
     }

     return resourcesObj;
  }

  return (
    <AppScreen style={[styles.container,{backgroundColor:state.screenBackgroundColor}]}> 
    <DialogInput isDialogVisible={state.isDialogVisible}
            title={"Search City"}
            message={"Enter city name"}
            hintInput ={"eg. New York"}
            submitInput={ (inputText) => {SetScreenData(inputText)} }
            closeDialog={ () => {SetScreenData()}}>
    </DialogInput>
     <Spinner
				visible={state.isLoading}
				textContent={'Loading...'}
			/>   
        <AppText style={{color:'white',fontSize:12,alignSelf:'center'}}>{ state.lastTimeUpdated}</AppText>
        <AppText style={{color:'white',fontSize:12,alignSelf:'center'}}>{state.currentDayWeatherForecast.City}</AppText>
        { !utils.NetworkConnection() &&
          <AppText style={{color:'red',fontSize:15,justifyContent:'center',alignSelf:'center'}}>Offline</AppText>
        }
       
      <ImageBackground source={state.weatherImageBackground} style={{width: '100%', height: Dimensions.get("window").height/2}}>
      
        <View style={{position: 'absolute', top: 0, left: 0, right: 0, bottom: 50, justifyContent: 'center', alignItems: 'center'}}>
          <AppText style={{color:'white',fontSize:50,fontWeight:'bold'}}>{state.currentDayWeatherForecast.CurrentTemperature}&deg;</AppText>
          <AppText style={{color:'white',fontSize:30,fontWeight:'bold'}}>{state.currentDayWeatherForecast.Description}</AppText>
        </View>
      </ImageBackground>  
      <View style={{justifyContent:'space-evenly',flexDirection:'row',height:50,width:Dimensions.get("window").width}}>
        <View style={{flexDirection:'column',height:50,width:50,alignSelf:'flex-start',justifyContent:'center'}}>
           <AppText style={{color:'white'}}>{state.currentDayWeatherForecast.MinTemperature}&deg;</AppText>
           <AppText style={{color:'white'}}>min</AppText>
        </View>
        <View style={{flexDirection:'column',height:50,width:100,justifyContent:'center'}}>
    <AppText style={{color:'white',alignSelf:'center'}}>{state.currentDayWeatherForecast.CurrentTemperature}&deg;</AppText>
         <AppText style={{color:'white',alignSelf:'center'}}>current</AppText>
        </View>
        <View style={{flexDirection:'column',width:50,height:50,alignSelf:'flex-end'}}>
        <AppText style={{color:'white',alignSelf:'center'}}>{state.currentDayWeatherForecast.MaxTemperature}&deg;</AppText>
        <AppText style={{color:'white',alignSelf:'center'}}>max</AppText>
        </View>
      </View> 
      <View style={{height:2,width:'100%',marginTop:5,backgroundColor:'white'}}/>
      <FlatList
          data={state.data}
          renderItem={({ item, index }) => (
          <View style={{flex:1,flexDirection:'row'}}>
            <AppText style={{color:'white',width:150,marginLeft:50,marginTop:20}}>{item.Day}</AppText>
            <Image source={item.Icon === 'partlySunny' ? require('../../../assets/partlysunny.png') : item.Icon === 'rain' ?  require('../../../assets/rain.png') : require('../../../assets/clear.png') } style={{marginTop:20,height:20,width:20}}></Image>
            <AppText style={{color:'white',flex:1,textAlign:'center' ,marginLeft:50,marginTop:20}}>{item.Temperature.toString()}&deg;</AppText>
          </View>
          )}
          keyExtractor={(item) => item.Id.toString()}
        />  
    </AppScreen>
  );
};

const styles = StyleSheet.create({
  container: {flexDirection:'column'},
});

export default Home;
