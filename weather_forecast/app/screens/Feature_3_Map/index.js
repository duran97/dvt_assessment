import React , {useEffect,useState} from "react";
import {  StyleSheet} from "react-native";
import MapView, {Marker  } from 'react-native-maps'; 
import {GetAllUserFavourites} from '../../utils/databaseOperation';

import AppScreen from '../../components/AppScreen';

const Feature_3 = () => {
	const [state, setState] = useState({
      data: [],
      region: {},
      favouriteMarkers: []
	  })

	  useEffect(() => {   
		  RetrieveFavourites();
		return () => {
		};
	  }, []);

	  function RetrieveFavourites (){
      GetAllUserFavourites().then(function (allFavourites) {
        
        let markerArr = [];
        // Massage the data, into new object.
        for (let index = 0; index < allFavourites.length; index++) {
          const favourite = allFavourites[index];
          
          let marker = {
            latitude: favourite.latitude,
            longitude: favourite.longitude,
            title: favourite.City
          }

          markerArr.push(marker);
      }

      setState({...state,favouriteMarkers:markerArr,region:markerArr[0]})
      
		}).catch(function (error) {
			alert(error);
		})
	  }

  return (
    <AppScreen style={styles.container}>
	   <MapView
      style={styles.map}
      >
        
        {state.favouriteMarkers.map((marker, index) => (
      <Marker 
          key={index}
          coordinate={{latitude:marker.latitude,longitude:marker.longitude}}
          pinColor = {"red"} // any color
          title={marker.title}
          description={''}/>
          ))}
      </MapView>
    </AppScreen>
  );
};

const styles = StyleSheet.create({
    container: {
        ...StyleSheet.absoluteFillObject,
        justifyContent: 'flex-end',
        alignItems: 'center',
      },
      map: {
        ...StyleSheet.absoluteFillObject,
      }
});

export default Feature_3;
