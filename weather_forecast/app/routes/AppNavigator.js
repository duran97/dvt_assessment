import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import HomeStack from './AppStackNavigator';
import FavouritesStack from './FavouritesStack';
import MapStack from '../../app/screens/Feature_3_Map';
import { createDrawerNavigator } from '@react-navigation/drawer';

const Drawer = createDrawerNavigator();

const AppNavigator = () => {
  return (
    <NavigationContainer>
      <Drawer.Navigator initialRouteName="Home">
        <Drawer.Screen name="Home" component={HomeStack} />
        <Drawer.Screen name="Favourites" component={FavouritesStack} />
        <Drawer.Screen name="View Map" component={MapStack} />
      </Drawer.Navigator>
    </NavigationContainer>
  );
}

export default AppNavigator;
 