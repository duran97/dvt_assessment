import { createStackNavigator } from "@react-navigation/stack";
import * as React from "react";
import { Dimensions, StyleSheet, View , TouchableHighlight,Image} from "react-native";
import { useNavigation } from "@react-navigation/native";

import HomeScreen from '../screens/Feature_1_Weather_Forecast';
// Components.
import AppText from "../components/AppText";

// Create stack hook.
const Stack = createStackNavigator();

const windowWidth = Dimensions.get("window").width;
const iconSize = 24;

// Header to be used on stack.
const HeaderTitle = ({ title }) => {
	return (
		<View style={styles.container}>
			<AppText style={styles.headerTitle} numberOfLines={1}>
				{title}
			</AppText>
		</View>
	);
};

// Header logo with icon.
const HeaderLogo = () => {
	const navigation = useNavigation();
	return (
		
		<TouchableHighlight
			underlayColor="white"
			onPress={() => navigation.openDrawer()}	
			style={{width:80,height:40,marginTop:15,alignItems:'flex-start'}}					  
		>
		<View>
			<Image style={{width:80,height:30}} source={require('../../assets/hamburger.png')}></Image>
		</View>
		</TouchableHighlight>				
	);
};

// Create about stack.
const HomeStack = () => {
	return (
		<Stack.Navigator
			screenOptions={{
				headerTitleAlign: "center",
				headerShown: true
			}}
		>
			<Stack.Screen
				name="Home"
				component={HomeScreen}
				options={{
					headerTitle: () => <HeaderTitle title="Weather" />,
					headerLeft: () => <HeaderLogo />
				}}
			/>
		</Stack.Navigator>
	);
};

// Styles.
const styles = StyleSheet.create({
	container: {
		alignItems: "center",
		flex: 1,
		flexDirection: "row",
		height: "100%",
	},
	headerTitle: {
		color: "black",
		fontSize: 24,
		letterSpacing: 1,
		overflow: "hidden",
		width: windowWidth - iconSize * 2 - 20,
		textAlign: "center",
	},
	icon: {
		marginLeft: 10,
	},
});

export default HomeStack;
