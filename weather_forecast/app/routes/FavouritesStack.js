import { createStackNavigator } from "@react-navigation/stack";
import * as React from "react";
import { Dimensions, StyleSheet, View } from "react-native";
import FavouriteScreen from '../screens/Feature_2_Favourites';

// Components.
import AppText from "../components/AppText";

// Create stack hook.
const Stack = createStackNavigator();

const windowWidth = Dimensions.get("window").width;
const iconSize = 24;

// Header to be used on stack.
const HeaderTitle = ({ title }) => {
	return (
		<View style={styles.container}>
			<AppText style={styles.headerTitle} numberOfLines={1}>
				{title}
			</AppText>
		</View>
	);
};

// Create about stack.
const FavouriteStack = () => {
	return (
		<Stack.Navigator
			screenOptions={{
				headerTitleAlign: "center",
				headerShown: true
			}}
		>
			<Stack.Screen
				name="Favourites"
				component={FavouriteScreen}
				options={{
					headerTitle: () => <HeaderTitle title="Favourites" />,
					
				}}
			/>
		</Stack.Navigator>
	);
};

// Styles.
const styles = StyleSheet.create({
	container: {
		alignItems: "center",
		flex: 1,
		flexDirection: "row",
		height: "100%",
	},
	headerTitle: {
		color: "black",
		fontSize: 24,
		letterSpacing: 1,
		overflow: "hidden",
		width: windowWidth - iconSize * 2 - 20,
		textAlign: "center",
	},
	icon: {
		marginLeft: 10,
	},
});

export default FavouriteStack;
