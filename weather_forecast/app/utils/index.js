import {Platform, PermissionsAndroid} from 'react-native';
import Geolocation from 'react-native-geolocation-service';
import moment from 'moment';
import NetInfo from '@react-native-community/netinfo';

//Components
import {OPEN_WEATHER_URL} from '../api';

const API_KEY = () => {
    return ''
}


const NetworkConnection = () => {
  // Determines if app is in network connection.
  let isConnection = ' ';

  const unsubscribe = NetInfo.addEventListener(state => {
    isConnection = state.isConnected;
  });

  return isConnection;
};

export const GetUserCurrentLocation = async () =>
  new Promise((resolve, reject) => {
    let watchId = null;
    try {
      watchId = Geolocation.getCurrentPosition(
        position => {
          resolve(position.coords);
        },
        error => {
          reject(error.message + '\nPlease restart application and enable location settings.')
        },
        {
          enableHighAccuracy: true,
          timeout: 1000,
          maximumAge: 0,
          distanceFilter: 0,
        },
      );
    } catch (error) {
      reject(error);
    }
  });

async function GetCurrentWeatherForecast(longitude, latitude) {
  try {
    let response = await fetch(
        OPEN_WEATHER_URL+
        'weather?lat=' +
        latitude +
        '&lon=' +
        longitude +
        '&units=metric&appid=6c7eb352c2cf459010eb69f05f8fcc36',
    );

    let responseJson = await response.json();

    return responseJson;
  } catch (error) {
    console.error(error);
  }
}

  async function Get5DayForecast(longitude,latitude,iconType) {
	try {
		let response = await fetch(
        OPEN_WEATHER_URL+
        'forecast?lat='+
        latitude+
        '&lon='+longitude+
        '&units=metric&appid=6c7eb352c2cf459010eb69f05f8fcc36',
		);
  
		// Retrieve the raw data list.
		let responseJson = await response.json();

		let weatherForecast5days = [];

		// Check if the date time is @ midnight
		for (let index = 0; index < responseJson.list.length; index++) {
			const weatherForecast = responseJson.list[index];
			
			if(weatherForecast.dt_txt.includes('00:00:00')){
				// extract the Weather and Main tag, Add to new object.
				weatherForecast5days.push({
					Id:	 weatherForecast.dt_txt,
					Day: GetDayOfWeek(weatherForecast.dt_txt),
					Temperature:Math.round(weatherForecast.main.temp),
					Icon:iconType
				})
			}
		}

		return weatherForecast5days;
		
	  } catch (error) {
		console.error(error);
	  }
  }

  function GetCitySearchResult(citySearchFilter) {
    return fetch(
      OPEN_WEATHER_URL+
      'weather?q='+
       citySearchFilter+
      '&units=metric&appid=6c7eb352c2cf459010eb69f05f8fcc36')
      .then((response) => response.json())
      .then((responseJson) => {

        console.log('GetCitySearchResult response',responseJson)

        return responseJson;
      }).catch((error) => {

        console.error(error);

      });

  }

function GetDayOfWeek(date) {
  // Retrieve the day of week from the full date string.
  var dateFormatted = moment(date, 'YYYY-MM-DD HH:mm:ss');

  return dateFormatted.format('dddd');
}

export default {
  GetCurrentWeatherForecast,
  Get5DayForecast,
  GetDayOfWeek,
  NetworkConnection,
  GetCitySearchResult
}
