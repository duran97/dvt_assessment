import Realm from 'realm';
import moment from 'moment';

export const FAVOURITES = "Favourites";
export const CURRENT_WEATHER = "CurrentWeatherForecast";
export const CURRENT_5_DAY_FORECAST = "Weather5DayForecast";

export const Favourites = {
  name: FAVOURITES,
  properties: {
    Id:'int',
    Country: 'string',
    City:'string',
    longitude: 'double',
    latitude: 'double'
  }
}

export const Weather5DayForecast = {
  name: CURRENT_5_DAY_FORECAST,
  properties: {
    Id:'string',
    Temperature:'string',
    Day:'string',
    Icon:'string'
  }
}

export const CurrentWeatherForecast = {
  name: CURRENT_WEATHER,
  properties: {
    City:'string',
    Longitude: 'double',
    Latitude: 'double',
    Country:'string',
    CurrentTemperature:'string',
    Description:'string',
    MinTemperature:'string',
    MaxTemperature:'string',
    LastTimeUpdated:'string',
    WeekForecast:'Weather5DayForecast[]'
  }
}

const databaseOperations = {
  schema: [Favourites,CurrentWeatherForecast,Weather5DayForecast],
  schemaVersion: 6
}

export const InsertFavourite = (newFavourite) => new Promise((resolve, reject) => {
  Realm.open(databaseOperations).then(realm => {
    realm.write(() => {

        let favourite = realm.objects(FAVOURITES).filtered('Id == $0', newFavourite.Id)[0]  ;
    
        console.log('In if Insert', favourite)

        realm.create(FAVOURITES, {
          Id:newFavourite.Id,
          Country:newFavourite.Country,
          City:newFavourite.City,
          longitude:newFavourite.longitude,
          latitude:newFavourite.latitude      
        });
     
      favourite = realm.objects(FAVOURITES).filtered('Id == $0', newFavourite.Id)[0];
      console.log('insert valued', favourite)
      resolve()
    });
  }).catch((error) => {
    reject(error)
  });
})

export const InsertCurrentForecast = (currentForecast,weather5dayForecast) => new Promise((resolve, reject) => {
  Realm.open(databaseOperations).then(realm => {
    realm.write(() => {

      // Retrieve all contents in SQLite.
      let currentItem = realm.objects(CURRENT_WEATHER);

      if(currentItem.length > 0){

        console.log('DELETE',currentItem)
         // Delete contents if an item exists.
        realm.delete(currentItem);

      }

      // Insert current forecast into sqlite.
      realm.create(CURRENT_WEATHER, {
        City:currentForecast.name,
        Longitude: currentForecast.coord.lon,
        Latitude: currentForecast.coord.lat,
        Country: currentForecast.sys.country,
        CurrentTemperature:currentForecast.main.temp.toString(),
        Description:currentForecast.weather[0].main,
        MinTemperature:currentForecast.main.temp_min.toString(),
        MaxTemperature:currentForecast.main.temp_max.toString(),
        LastTimeUpdated:moment().format('MMMM Do YYYY, h:mm:ss a'),
        WeekForecast:[]   
      });

      // Retrieve record inserted into sqlite. 
      let insertedItem = realm.objects(CURRENT_WEATHER)[0];

      // Loop through the 5 day forecast and push into sqlite record.
      weather5dayForecast.forEach(element => {
           insertedItem.WeekForecast.push({
             Id:element.Id,
             Temperature:Math.round(element.Temperature).toString(),
             Day:element.Day,
             Icon:element.Icon
           })
      });

      resolve()
    });
  }).catch((error) => {
    reject(error)
  });
})


export const GetAllUserFavourites = () => new Promise((resolve, reject) => {

  Realm.open(databaseOperations).then(realm => {

    let allUserFavourites = realm.objects(FAVOURITES);

    console.log('GetAllUserFavourites',allUserFavourites)
    resolve(allUserFavourites)
  }).catch((error) => {
    reject(error)
  })
})

export const GetCurrentForecast = () => new Promise((resolve, reject) => {
  Realm.open(databaseOperations).then(realm => {

    let currentForecast = realm.objects(CURRENT_WEATHER)[0];

    console.log('currentForecast',currentForecast)
    resolve(currentForecast)
  }).catch((error) => {
    reject(error)
  })
})

export default new Realm(databaseOperations);